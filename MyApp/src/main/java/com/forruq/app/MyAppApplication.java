package com.forruq.app;

import com.forruq.app.database.entity.ERole;
import com.forruq.app.database.entity.RoleEntity;
import com.forruq.app.database.entity.UserEntity;
import com.forruq.app.database.repository.AuthRepository;
import com.forruq.app.database.repository.RoleRepository;
import com.forruq.app.exceptions.CustomDataIntegrityViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

//@SpringBootApplication
//public class MyAppApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(MyAppApplication.class, args);
//	}
//
//}


@SpringBootApplication
public class MyAppApplication implements CommandLineRunner {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AuthRepository authRepository;

	@Autowired
	private PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(MyAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		 long roleCount =	roleRepository.count();

		if(roleCount < 1) {

			RoleEntity role = new RoleEntity();
			role.setName(ERole.ROLE_ADMIN);
			roleRepository.save(role);

			RoleEntity role2 = new RoleEntity();
			role2.setName(ERole.ROLE_USER);
			roleRepository.save(role2);

		}


		Set<RoleEntity> roles = new HashSet<>();
		RoleEntity userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
				.orElseThrow(() -> new CustomDataIntegrityViolationException("Error: Role does not found."));
		roles.add(userRole);

		UserEntity user = new UserEntity();
		user.setFirstName("Forruq");
		user.setLastName("Ahammed");
		user.setRoles(roles);
		user.setEmail("forruq@gmail.com");
		user.setMobile("01442232132");
		user.setUserId("forruq");
		user.setPassword(encoder.encode("forruq"));
		user.setDob(new Date());

		if(!authRepository.existsByEmailAndIsDeleted(user.getEmail(), false)){
			authRepository.save(user);
		}


	}
}
