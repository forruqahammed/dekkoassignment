package com.forruq.app.controller;


import com.forruq.app.dto.request.auth.LoginRequest;
import com.forruq.app.dto.request.auth.SignupRequest;
import com.forruq.app.dto.response.auth.LoginResponse;
import com.forruq.app.dto.response.common.CommonResponse;
import com.forruq.app.service.AuthService;
import com.forruq.app.utils.ApiUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = ApiUrl.SERVER_API, allowedHeaders = "*")
@RequestMapping(ApiUrl.BASE_API + ApiUrl.AUTH)
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping(ApiUrl.REGISTRATION)
    public ResponseEntity<CommonResponse> registerUser(@RequestBody SignupRequest signUpRequest) {

        CommonResponse response = authService.signup(signUpRequest);
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    @PostMapping(ApiUrl.LOGIN)
    public ResponseEntity<LoginResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {

        LoginResponse response = authService.login(loginRequest);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping(ApiUrl.LOGOUT)
    public ResponseEntity<CommonResponse> logout() {
        CommonResponse response = authService.logout();
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
