package com.forruq.app.controller;


import com.forruq.app.database.entity.UserEntity;
import com.forruq.app.dto.request.auth.UpdateUserRequest;
import com.forruq.app.dto.request.common.CustomPageRequest;
import com.forruq.app.dto.response.common.CommonResponse;
import com.forruq.app.service.AuthService;
import com.forruq.app.utils.ApiUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@CrossOrigin(origins = ApiUrl.SERVER_API, allowedHeaders = "*")
@RequestMapping(ApiUrl.BASE_API + ApiUrl.PROFILE)
public class UserController {

    @Autowired
    private AuthService authService;


    @PostMapping(ApiUrl.UPDATE)
    private ResponseEntity<CommonResponse> updateProfile(@RequestBody UpdateUserRequest request)  {
        CommonResponse response = authService.updateProfile(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping(ApiUrl.DELETE_BY_ID)
    private ResponseEntity<CommonResponse> deleteProfile(@PathVariable("oid") String oid) {
        CommonResponse response = authService.deleteProfile(oid);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping()
    private ResponseEntity<Page<UserEntity>> profileList(@RequestBody CustomPageRequest request) {
        Page<UserEntity> response = authService.userList(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(ApiUrl.DETAILS_BY_ID)
    private ResponseEntity<UserEntity> getProfileDetails(@PathVariable("oid") String oid) {
        UserEntity response = authService.getProfileDetails(oid);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
