package com.forruq.app.database.entity;

public enum ERole {
    ROLE_ADMIN, ROLE_USER
}
