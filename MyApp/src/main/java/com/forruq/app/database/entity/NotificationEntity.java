package com.forruq.app.database.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "notification")
public class NotificationEntity extends BaseEntity{

	private String title;

	private String content;

	private boolean isSeen;

	@ManyToOne
	private UserEntity user;

}
