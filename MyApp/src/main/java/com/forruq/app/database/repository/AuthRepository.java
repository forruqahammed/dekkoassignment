package com.forruq.app.database.repository;

import com.forruq.app.database.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface AuthRepository extends ServiceRepository<UserEntity> {

    Optional<UserEntity> findByEmailAndIsDeleted(String email, boolean isDeleted);
    Optional<UserEntity> findByUserIdAndIsDeleted(String userId, boolean isDeleted);
    Boolean existsByEmailAndIsDeleted(String email, boolean isDeleted);


    Boolean existsByUserIdAndIsDeleted(String userId, boolean isDeleted);

    Page<UserEntity> findByIsDeletedAndFirstNameIgnoreCaseContaining(boolean isDeleted, String name, Pageable pageable);

}
