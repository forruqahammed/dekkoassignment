package com.forruq.app.database.repository;

import com.forruq.app.database.entity.ERole;
import com.forruq.app.database.entity.RoleEntity;

import java.util.Optional;

public interface RoleRepository extends ServiceRepository<RoleEntity> {

    Optional<RoleEntity> findByName(ERole name);
}
