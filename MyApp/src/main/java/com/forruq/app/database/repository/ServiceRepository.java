package com.forruq.app.database.repository;

import com.forruq.app.database.entity.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ServiceRepository<E extends BaseEntity> extends JpaRepository<E, String>, JpaSpecificationExecutor<E> {
    Optional<E> findByOidAndIsDeleted(String oid, boolean isDeleted);
    Page<E> findByIsDeletedOrderByCreatedOnDesc(boolean isDeleted, Pageable pageable);

    Boolean existsByOidAndIsDeleted(String oid, boolean isDeleted);
    long countByIsDeleted(boolean isDeleted);


}