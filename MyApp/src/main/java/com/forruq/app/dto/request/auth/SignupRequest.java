package com.forruq.app.dto.request.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Date;

@Data
public class SignupRequest {
    @NotBlank
    @Size(max = 30)
    private String firstName;

    @NotBlank
    @Size(max = 30)
    private String lastName;

    @NotBlank
    @Size(max = 50)
    @Email
    @Column(updatable = false)
    private String email;

    @Size(max = 50)
    private String userId;

    @NotBlank
    @Size(max = 120)
//    @JsonIgnore
    private String password;

    @Size(max = 20)
    private String mobile;

    private String address;

    private Date dob;
}
