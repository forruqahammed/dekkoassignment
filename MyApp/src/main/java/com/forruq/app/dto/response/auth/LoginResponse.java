package com.forruq.app.dto.response.auth;

import lombok.Data;

import java.util.List;


@Data
public class LoginResponse {
    private String token;
    private String type = "Bearer";
    private String oid;
    private String firstName;
    private String lastName;
    private String email;
    private List<String> roles;

    public LoginResponse(String accessToken, String oid, String firstName, String lastName, String email, List<String> roles) {
        this.token = accessToken;
        this.oid = oid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.roles = roles;
    }
}
