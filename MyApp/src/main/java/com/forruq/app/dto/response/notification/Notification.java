package com.forruq.app.dto.response.notification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Notification {
	private String oid;

	private String title;

	private String content;

	private String firstName;

	private String userId;

	private boolean isSeen;

	private Date createdOn;


}
