package com.forruq.app.exceptions;

import com.forruq.app.utils.StatusCode;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	private final String INCORRECT_REQUEST = "INCORRECT_REQUEST";
	private final String BAD_REQUEST = "BAD_REQUEST";
	private final String CONFLICT = "CONFLICT";

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleUserNotFoundException(RecordNotFoundException ex,
			WebRequest request) {
		String message =  ex.getLocalizedMessage();
		ErrorResponse error = new ErrorResponse(StatusCode.BAD_REQUEST, message);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(MissingHeaderInfoException.class)
	public final ResponseEntity<ErrorResponse> handleInvalidTraceIdException(MissingHeaderInfoException ex,
			WebRequest request) {
		String message  =  ex.getLocalizedMessage();
		ErrorResponse error = new ErrorResponse(StatusCode.BAD_REQUEST, message);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<ErrorResponse> handleConstraintViolation(ConstraintViolationException ex,
			WebRequest request) {
		String message = ex.getMessage();
		ErrorResponse error = new ErrorResponse(StatusCode.BAD_REQUEST, message);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustomDataIntegrityViolationException.class)
	public final ResponseEntity<ErrorResponse> dataIntegrityViolationException(CustomDataIntegrityViolationException ex,
			WebRequest request) {
		String message = ex.getLocalizedMessage();
		ErrorResponse error = new ErrorResponse(StatusCode.NOT_FOUND, message);
		return new ResponseEntity<>(error, HttpStatus.OK);
	}
}