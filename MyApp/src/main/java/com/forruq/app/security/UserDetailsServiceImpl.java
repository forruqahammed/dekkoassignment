package com.forruq.app.security;

import com.forruq.app.database.entity.UserEntity;
import com.forruq.app.database.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Transactional
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	AuthRepository authRepository;

//	@Override
//	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//		UserEntity user = authRepository.findByEmailAndIsDeleted(email, "NO")
//				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with email : " + email));
//
//		return UserPrinciple.build(user);
//	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity user = new UserEntity();
		Optional<UserEntity> optionalUser = authRepository.findByEmailAndIsDeleted(email, false);

		Optional<UserEntity> optionalUser2 = authRepository.findByUserIdAndIsDeleted(email, false);

		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		} else if (optionalUser2.isPresent()) {
			user = optionalUser2.get();
		} else {
			throw new UsernameNotFoundException("User Not Found with userid or email : " + email);
		}
		return UserPrinciple.build(user);
	}

}


