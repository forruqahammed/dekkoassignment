package com.forruq.app.service;

import com.forruq.app.database.entity.UserEntity;
import com.forruq.app.dto.request.auth.LoginRequest;
import com.forruq.app.dto.request.auth.SignupRequest;
import com.forruq.app.dto.request.auth.UpdateUserRequest;
import com.forruq.app.dto.request.common.CustomPageRequest;
import com.forruq.app.dto.response.auth.LoginResponse;
import com.forruq.app.dto.response.common.CommonResponse;
import org.springframework.data.domain.Page;


public interface AuthService {

    public CommonResponse signup(SignupRequest request);

    public CommonResponse updateProfile(UpdateUserRequest request);

    public CommonResponse deleteProfile(String oid);

    public Page<UserEntity> userList(CustomPageRequest request);

    public UserEntity getProfileDetails(String oid);

    public LoginResponse login(LoginRequest request);

    public CommonResponse logout();

}
