package com.forruq.app.service;

import com.forruq.app.dto.request.common.CustomPageRequest;
import com.forruq.app.dto.response.common.CommonResponse;
import org.springframework.data.domain.Page;

public interface CommonService<T> {

    public CommonResponse save(T request);

    public CommonResponse update(T request);

    public CommonResponse delete(String oid);

    public T detailsByOid(String oid);

    public Page<T> getList(CustomPageRequest request);
}
