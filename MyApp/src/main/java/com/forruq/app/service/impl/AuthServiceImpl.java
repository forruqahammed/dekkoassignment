package com.forruq.app.service.impl;

import com.forruq.app.database.entity.ERole;
import com.forruq.app.database.entity.RoleEntity;
import com.forruq.app.database.entity.UserEntity;
import com.forruq.app.database.repository.AuthRepository;
import com.forruq.app.database.repository.RoleRepository;
import com.forruq.app.dto.request.auth.LoginRequest;
import com.forruq.app.dto.request.auth.SignupRequest;
import com.forruq.app.dto.request.auth.UpdateUserRequest;
import com.forruq.app.dto.request.common.CustomPageRequest;
import com.forruq.app.dto.response.auth.LoginResponse;
import com.forruq.app.dto.response.common.CommonResponse;
import com.forruq.app.exceptions.CustomDataIntegrityViolationException;
import com.forruq.app.exceptions.RecordNotFoundException;
import com.forruq.app.security.JwtUtils;
import com.forruq.app.security.UserPrinciple;
import com.forruq.app.service.AuthService;
import com.forruq.app.service.WebSocketService;
import com.forruq.app.utils.CustomMessage;
import com.forruq.app.utils.StatusCode;
import com.forruq.app.utils.Topics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private WebSocketService webSocketService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public void sendNotification(String message) {
        messagingTemplate.convertAndSend("/topic/notifications", message);
    }

    @Override
    public CommonResponse signup(SignupRequest request) {
        if (authRepository.existsByEmailAndIsDeleted(request.getEmail(), false)) {
            return new CommonResponse(StatusCode.BAD_REQUEST, "Email is already in use!");
        } else if (authRepository.existsByUserIdAndIsDeleted(request.getUserId(), false)) {
            return new CommonResponse(StatusCode.BAD_REQUEST, "User id is already in use!");
        }

        if (request.getFirstName() == null || (request.getFirstName().equals(""))) {
            log.info("Inside Auth service registration method and first name is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "First name is empty");
        } else  if (request.getLastName() == null || (request.getLastName().equals(""))) {
            log.info("Inside Auth service registration method and last name is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "Last name is empty");
        }else if (request.getEmail() == null || (request.getEmail().equals(""))) {
            log.info("Inside Auth service registration method and email is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "User email is empty");
        }  else  if (request.getMobile() == null || (request.getMobile().equals(""))) {
            log.info("Inside Auth service registration method and mobile is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "Mobile number is empty");
        }  else if (request.getUserId() == null || (request.getUserId().equals(""))) {
            log.info("Inside Auth service registration method and user id is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "User id is empty");
        }  else if (request.getPassword() == null || (request.getPassword().equals(""))) {
            log.info("Inside Auth service registration method and Password is empty");
            return new CommonResponse(StatusCode.NO_CONTENT, "User Password is empty");
        }

        UserEntity user = new UserEntity();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setMobile(request.getMobile());
        user.setAddress(request.getAddress());
        user.setEmail(request.getEmail());
        user.setUserId(request.getUserId());
        user.setPassword(encoder.encode(request.getPassword()));
        user.setDob(request.getDob());


        Set<RoleEntity> roles = new HashSet<>();

        RoleEntity userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new CustomDataIntegrityViolationException("Error: Role does not found."));

        roles.add(userRole);
        user.setRoles(roles);
        try {
            authRepository.save(user);
            webSocketService.sendNotification("Data has been saved!");
            log.info("Inside Auth service registration method and user registered successfully! ");
        } catch (Exception e) {
            log.info("Inside Auth service registration method and user registration failed due to Exception! ");
            return new CommonResponse(StatusCode.BAD_REQUEST, "User registration failed!");
        }
        return new CommonResponse(StatusCode.CREATED, CustomMessage.REGISTRATION_SUCCESS );
    }



    @Override
    public CommonResponse updateProfile(UpdateUserRequest request) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        UserEntity userEntity = new UserEntity();
        Optional<UserEntity> optionalUser = authRepository.findByOidAndIsDeleted(request.getOid(), false);


        if(optionalUser.isPresent()){
            userEntity = optionalUser.get();
        } else {
            log.info("Inside User service update method and No record found by id : " + request.getOid());
            return new CommonResponse(StatusCode.BAD_REQUEST, CustomMessage.NO_RECORD_FOUND + request.getOid());
        }


        if(request.getFirstName() != null){
            userEntity.setFirstName(request.getFirstName());
        }
        if(request.getLastName() != null){
            userEntity.setLastName(request.getLastName());
        }
        if(request.getAddress() != null){
            userEntity.setAddress(request.getAddress());
        }
        if(request.getDob() != null ){
            userEntity.setDob(request.getDob());
        }
        if(request.getMobile() != null){
            userEntity.setMobile(request.getMobile());
        }

        userEntity.setUpdatedOn(new Date());
        userEntity.setUpdatedBy(userPrinciple.getOid());

        try {
            authRepository.save(userEntity);
            log.info("Inside User service update method and updated successfully!");
        } catch (Exception e) {
            log.info("Inside User service update method and update failed due to Exception");
            throw new DataIntegrityViolationException(Topics.USER.getName() + CustomMessage.UPDATE_FAILED_MESSAGE);
        }
        return new CommonResponse(StatusCode.CREATED, Topics.USER.getName() + CustomMessage.UPDATE_SUCCESS_MESSAGE);
    }


    @Override
    public CommonResponse deleteProfile(String oid) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        UserEntity userEntity = new UserEntity();
        Optional<UserEntity> optionalUser = authRepository.findByOidAndIsDeleted(oid, false);

        if(optionalUser.isPresent()){
            userEntity = optionalUser.get();
        } else {
            log.info("Inside User service update method and No record found by id : " + oid);
            return new CommonResponse(StatusCode.BAD_REQUEST, CustomMessage.NO_RECORD_FOUND + oid);
        }

        userEntity.setDeleted(true);
        userEntity.setUpdatedBy(userPrinciple.getOid());
        userEntity.setUpdatedOn(new Date());

        try {
            authRepository.save(userEntity);
            log.info("Inside User service delete method and deleted successfully");
        } catch (Exception e) {
            log.info("Inside User service delete method and delete failed due to Exception");
            return new CommonResponse(StatusCode.BAD_REQUEST, Topics.USER.getName() + CustomMessage.DELETE_FAILED_MESSAGE);
        }
        return new CommonResponse(StatusCode.CREATED, Topics.USER.getName() + CustomMessage.DELETE_SUCCESS_MESSAGE);
    }

    @Override
    public Page<UserEntity> userList(CustomPageRequest request) {
        if (request.getSize() < 1 && request.getPageNo() == 0) {
            log.info("Inside User service get list method where size is less than 1 and page no is 0");
            int count = (int) authRepository.countByIsDeleted(false);
            request.setSize(count == 0 ? 10 : count);
        }
        Pageable pageable = PageRequest.of(request.getPageNo(), request.getSize());
        log.info("Inside User service get list method");

        return authRepository.findByIsDeletedOrderByCreatedOnDesc(false, pageable);
    }

    @Override
    public UserEntity getProfileDetails(String oid) {
        Optional<UserEntity> optionalArea = authRepository.findByOidAndIsDeleted(oid, false);

        if(optionalArea.isEmpty()){
            log.info("Inside auth service get profile details method and no record found by oid : " + oid);
            throw new RecordNotFoundException(CustomMessage.NO_RECORD_FOUND + oid);
        }

        return optionalArea.get();
    }

    @Override
    public LoginResponse login(LoginRequest request) {

        String message = "Please enter your valid user and password";

        if (request.getEmail() == null) {
            log.info("Please enter your valid Email address");
            message = "Email is empty";
        } else if (request.getPassword() == null) {
            log.info("Inside login method and password is empty");
            message = "Please enter your valid password";
        }

        UserEntity userEntity = new UserEntity();

        if (authRepository.existsByEmailAndIsDeleted(request.getEmail(), false) || authRepository.existsByUserIdAndIsDeleted(request.getEmail(), false)) {
            Optional<UserEntity> optionalUser = authRepository.findByEmailAndIsDeleted(request.getEmail(), false);

            Optional<UserEntity> optionalUser2 = authRepository.findByUserIdAndIsDeleted(request.getEmail(), false);

            if (optionalUser.isPresent()) {
                userEntity = optionalUser.get();
            } else if (optionalUser2.isPresent()) {
                userEntity = optionalUser2.get();
            }

            if (userEntity.isLocked()) {
                message = "Inside auth service and account locked";
                throw new CustomDataIntegrityViolationException("Your account has been locked, please contact with authorities");
            } else if(userEntity.isLoggedIn()){
                message = "Inside auth service and already logged in";
                throw new CustomDataIntegrityViolationException("You are already logged in with another device.");
            }


        } else {
            message = "Please enter your valid email and password";
            throw new CustomDataIntegrityViolationException(message);
        }


        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
                    .collect(Collectors.toList());
            log.info("Inside login method and login success!");
            loginAction(userEntity);
            return new LoginResponse(jwt, userDetails.getOid(), userDetails.getFirstName(), userDetails.getLastName(), userDetails.getUsername(), roles);
        } catch (Exception e) {
            log.info("Inside login method and login failed due to exception");
            addAttempts(userEntity);
            throw new CustomDataIntegrityViolationException(message);
        }

    }

    @Override
    public CommonResponse logout() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        UserEntity userEntity = new UserEntity();
        Optional<UserEntity> optionalUser = authRepository.findByOidAndIsDeleted(userPrinciple.getOid(), false);

        if(optionalUser.isPresent()){
            userEntity = optionalUser.get();
        } else {
            log.info("Inside User service logout method and No record found by id : " + userPrinciple.getOid());
            return new CommonResponse(StatusCode.BAD_REQUEST, CustomMessage.NO_RECORD_FOUND + userPrinciple.getOid());
        }

        userEntity.setLoggedIn(false);

        try {
            authRepository.save(userEntity);
            logoutAction(userEntity);
            log.info("Inside User service logout method and logout successfully");
        } catch (Exception e) {
            log.info("Inside User service logout method and logout failed due to Exception");
            return new CommonResponse(StatusCode.BAD_REQUEST, "Logout failed");
        }
        return new CommonResponse(StatusCode.CREATED, "Successfully log out");
    }


    @Async
    public void addAttempts(UserEntity user){
        int loginAttempts =  user.getLoginAttempts() + 1;
        user.setLoginAttempts(user.getLoginAttempts() + 1);

        if(loginAttempts > 3){
            user.setLocked(true);
        }
        authRepository.save(user);
    }

    @Async
    public void loginAction(UserEntity user){
        user.setLoginAttempts(0);
        user.setLoggedIn(true);
        authRepository.save(user);
    }

    @Async
    public void logoutAction(UserEntity user){
        user.setLoggedIn(false);
        authRepository.save(user);
    }


//    private Collection<? extends GrantedAuthority> getAuthorities(
//            Collection<RoleEntity> roles) {
//        List<GrantedAuthority> authorities
//                = new ArrayList<>();
//        for (RoleEntity role : roles) {
//            authorities.add(new SimpleGrantedAuthority(role.getName().toString()));
//        }
//
//        return authorities;
//    }

}
