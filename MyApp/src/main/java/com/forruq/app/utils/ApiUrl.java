package com.forruq.app.utils;

public abstract class ApiUrl {

	public static final String SERVER_API = "*";
	public static final String BASE_API = "/api/v1";

	//base endpoint
	public static final String PING = "/ping";

	public static final String AUTH = "/auth";

	public static final String PROFILE = "/profiles";

	public static final String NOTIFICATION = "/notifications";



	//Api endpoint
	public static final String SAVE = "/new";
	public static final String UPDATE = "/update";
	public static final String DELETE_BY_ID = "/delete/{oid}";
	public static final String DETAILS_BY_ID = "/details/{oid}";

	public static final String REGISTRATION = "/registration";
	public static final String LOGIN = "/login";

	public static final String LOGOUT = "/logout";

}
