package com.forruq.app.utils;

public class CustomMessage {
	
	public static final String SAVE_SUCCESS_MESSAGE = " has been saved successfully!";
	public static final String UPDATE_SUCCESS_MESSAGE = " has been updated successfully!";
	public static final String DELETE_SUCCESS_MESSAGE = " has been deleted successfully!";
	public static final String SAVE_FAILED_MESSAGE = " Failed to save";
	public static final String UPDATE_FAILED_MESSAGE = " Failed to update";
	public static final String DELETE_FAILED_MESSAGE = " Failed to delete";
	public static final String DATA_FOUND = "Data has been found";

	public static final String DATA_NOT_VALID = "Data is not valid";

	public static final String NO_RECORD_FOUND = "No record found for given value: ";

	public static final String ALREADY_EXIST = " already exist";

	public static final String TOKEN_ERROR = "Token not found";

	public static final String DELETE_PERMISSION_ERROR = "You have no permission to delete this record ";

	public static final String UPDATE_PERMISSION_ERROR = "You have no permission to update this record ";

	public static final String REGISTRATION_SUCCESS = "Registered Successfully!";


}
