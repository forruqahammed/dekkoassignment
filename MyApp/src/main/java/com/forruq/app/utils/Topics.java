package com.forruq.app.utils;

public enum Topics {

	USER("User", 101),
	NOTIFICATION("Notification", 102)
	;

	private final String name;

	private final int code;
	
	private Topics(String name, int code) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public int getCode() {
		return code;
	}

}
